package com.stronglover.MadrassaApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MadrassaAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MadrassaAppApplication.class, args);
	}

}
