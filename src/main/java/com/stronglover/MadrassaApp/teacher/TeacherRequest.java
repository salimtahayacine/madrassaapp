package com.stronglover.MadrassaApp.teacher;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class TeacherRequest {

    private final String fullname;
    private final String email;
    private final LocalDateTime birthDate;
    private final E_Metier matier;
    private final Double salaire;
}
