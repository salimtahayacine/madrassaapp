package com.stronglover.MadrassaApp.teacher;

import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PreDestroy;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/teacher")
public class teacherController {

    private final TeacherRepository teacherRepository;
    private final TeacherService teacherService;

    @GetMapping
    public List<Teacher> getAllTeachers(){
            return teacherRepository.findAll();
    }

    @GetMapping("/{id}")
    public Teacher getTeacher(@PathVariable String id){
        Teacher teacher = new Teacher();
        teacher =  teacherRepository.findById(id)
                .orElseThrow(()-> new IllegalStateException(" teacher dont founded"));
        return teacher;
    }

    @PreAuthorize("hasAnyRole('ROLE_TEACHER','ROLE_ADMIN')")
    @PostMapping
    public String addNewTeacher(@RequestBody TeacherRequest request){
        teacherService.AddTeacher(request);
        return request.getFullname()+" Added Successfully";
    }

    @PreAuthorize("hasAnyRole('ROLE_TEACHER','ROLE_ADMIN')")
    @DeleteMapping("/{id}")
    public String deleteTeacher(@PathVariable String id){
        Teacher teacher = new Teacher();
        teacher =  teacherRepository.findById(id)
                .orElseThrow(()-> new IllegalStateException(" teacher dont founded"));

        teacherRepository.deleteById(id);
        return String.format("Teacher with %s id Deleted Successfully",id);
    }
    @PreAuthorize("hasAnyRole('ROLE_TEACHER','ROLE_ADMIN')")
    @PutMapping("/{id}")
    public String updateTeacher(@PathVariable String id ,@RequestBody TeacherRequest request){
        teacherService.updateTeacher(id,request);
        return String.format("Teacher with %s id Updated Successfully",id);
    }

}
