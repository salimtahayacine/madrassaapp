package com.stronglover.MadrassaApp.teacher;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum E_Metier {
    INFORMATIQUE,
    MATH,
    PHYSIC,
    CHIMIE,
    SPORT;
}
