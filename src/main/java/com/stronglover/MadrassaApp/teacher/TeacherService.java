package com.stronglover.MadrassaApp.teacher;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TeacherService {

    private final TeacherRepository teacherRepository;


    public void AddTeacher(TeacherRequest request){
        //check the email is already taken or not
        boolean isEmailTaken = teacherRepository
                .findByEmail(request.getEmail())
                .isPresent();
        if(isEmailTaken){
            throw  new IllegalStateException("Email already taken ");
        }
        Teacher teacher = new Teacher();
        teacher.setFullname(request.getFullname());
        teacher.setEmail(request.getEmail());
        teacher.setSalaire(request.getSalaire());
        teacher.setBirthDate(request.getBirthDate());
        teacher.setMetier(request.getMatier());

        teacherRepository.save(teacher);
    }

    public void updateTeacher(String id,TeacherRequest request ){
        Teacher teacher = new Teacher();
         teacher = teacherRepository
                 .findById(id)
                 .orElseThrow(()-> new IllegalStateException("Teacher dnt founded"));

         teacher.setFullname(request.getFullname());
         teacher.setEmail(request.getEmail());
         teacher.setSalaire(request.getSalaire());
         teacher.setBirthDate(request.getBirthDate());
         teacher.setMetier(request.getMatier());

         teacherRepository.save(teacher);
    }
}
