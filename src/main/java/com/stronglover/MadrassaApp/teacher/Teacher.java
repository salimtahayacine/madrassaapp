package com.stronglover.MadrassaApp.teacher;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Document(collection = "teacher")
public class Teacher {

    private String id;
    private String fullname;
    private String email;
    private LocalDateTime birthDate;
    private Double salaire;

    private E_Metier metier ;

    public Teacher(String fullname,
                   String email,
                   LocalDateTime birthDate,
                   Double salaire,
                   E_Metier metier) {
        this.fullname = fullname;
        this.email = email;
        this.birthDate = birthDate;
        this.salaire = salaire;
        this.metier = metier;
    }
}
