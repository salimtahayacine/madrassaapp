package com.stronglover.MadrassaApp.appUser;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUSerRepository extends MongoRepository<AppUser, String> {


    Optional<AppUser> findByEmail(String email);
}
