package com.stronglover.MadrassaApp.appUser.registration;

import com.stronglover.MadrassaApp.appUser.AppUser;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/registration")
public class userRegistrationController {

    private final userRegistrationService registrationService;

    @PostMapping
    public String register(userRegistrationRequest request){
        registrationService.register(request,new AppUser());
        return "Registred";
    }
}
