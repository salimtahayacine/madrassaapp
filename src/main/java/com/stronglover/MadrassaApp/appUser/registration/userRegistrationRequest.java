package com.stronglover.MadrassaApp.appUser.registration;

import lombok.*;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class userRegistrationRequest {
    private final String fullname;
    private final String email;
    private final String password;
}
