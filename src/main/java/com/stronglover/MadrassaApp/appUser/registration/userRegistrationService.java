package com.stronglover.MadrassaApp.appUser.registration;

import com.stronglover.MadrassaApp.appUser.AppUser;
import com.stronglover.MadrassaApp.appUser.AppUserService;
import com.stronglover.MadrassaApp.appUser.E_Role;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class userRegistrationService {


    private final AppUserService appUserService;
    public String register(userRegistrationRequest request, AppUser appUser){
        appUser.setFullname(request.getFullname());
        appUser.setEmail(request.getEmail());
        appUser.setPassword(request.getPassword());
        appUser.setRole(E_Role.USER);
        appUserService.SignUpStudent(appUser);
        return ""+appUser;
    }
}
