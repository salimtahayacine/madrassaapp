package com.stronglover.MadrassaApp.appUser;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AppUserService implements UserDetailsService {

    private final AppUSerRepository appUSerRepository;
    private  final BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return appUSerRepository.findByEmail(email)
                .orElseThrow(()-> new IllegalStateException("Email Not Founded"));
    }
    public String SignUpStudent( AppUser appUser){
        boolean isEmailExist = appUSerRepository.findByEmail(appUser.getEmail()).isPresent();
        if (isEmailExist){
            throw  new IllegalStateException("Email Already Taken");
        }
        String encodedPassword = bCryptPasswordEncoder.encode(appUser.getPassword());
        appUser.setPassword(encodedPassword);
        appUser.setRole(E_Role.STUDENT);
        appUser.setEnabled(true);
        appUser.setLocked(false);

        appUSerRepository.save(appUser);
        return "Done Successfully"+appUser;
    }
}
