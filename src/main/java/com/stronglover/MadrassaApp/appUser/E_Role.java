package com.stronglover.MadrassaApp.appUser;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;



public enum E_Role {
    STUDENT(Set.of(Permission.STUDENT_READ,
                   Permission.STUDENT_WRITE
            )),
    TEACHER(Set.of(Permission.TEACHER_WRITE,
                   Permission.TEACHER_READ)),
    USER(Collections.emptySet()),
    ADMIN(Set.of(
            Permission.ADMIN_READ,
                    Permission.ADMIN_WRITE,
                    Permission.TEACHER_WRITE,
                    Permission.TEACHER_READ,
                    Permission.STUDENT_WRITE,
                    Permission.STUDENT_READ)
  ),
    MANAGER(Set.of())

    ;

    @Getter
    private final Set<Permission> permissions;

    E_Role(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public List<SimpleGrantedAuthority> getAuthorities() {
        var authorities = getPermissions()
                .stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toList());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return authorities;
    }
}
