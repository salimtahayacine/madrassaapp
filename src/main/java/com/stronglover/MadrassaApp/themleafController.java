package com.stronglover.MadrassaApp;

import com.stronglover.MadrassaApp.student.Student;
import com.stronglover.MadrassaApp.student.StudentRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class themleafController {

    private final StudentRepository studentRepository;

    public themleafController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/students")
    public String listStudents(Model model) {
        model.addAttribute("students", studentRepository.findAll());

        return "students";
    }
    @GetMapping("/students/new")
    public String createStudentForm(Model model) {

        // create student object to hold student form data
        Student student = new Student();
        model.addAttribute("student", studentRepository.save(student));
        return "create_student";

    }

    @GetMapping("/students/edit/{id}")
    public String editStudentForm(@PathVariable String id, Model model) {
        model.addAttribute("student", studentRepository.getStudentById(id).orElseThrow());
        return "edit_student";
    }

//    @DeleteMapping("/students/delete/{id}")
//    public String deleteStudent(@PathVariable String id, Model model){
//        studentRepository.deleteById(id);
//        model.addAttribute("student","deleted with "+id);
//
//        return "students";
//    }
}
