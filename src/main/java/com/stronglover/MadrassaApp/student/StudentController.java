package com.stronglover.MadrassaApp.student;

import lombok.AllArgsConstructor;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@AllArgsConstructor
//@RequestMapping("/api/v1/student")
public class StudentController {

    private final StudentRepository studentRepository;
    private final StudentService studentService;
    // handler method to handle list students and return mode and view

    @GetMapping
    public List<Student> getAllStudent(){
       return studentRepository.findAll();
    }

    @GetMapping("/{id}")
    public Student getStudent(@PathVariable("id") String id){
        return studentRepository.findById(id)
                .orElseThrow(()->new IllegalStateException("Student dnt Founded using this id:"+id));
    }

    @PostMapping
    public String AddStudent(@RequestBody  StudentRequest request){
        studentService.addStudent(request,new Student());
        return "Student Added"+request;
    }
    @DeleteMapping("/api/v1/student/{id}")
    public String deleteStudent(@PathVariable("id") String id){
        studentService.DeleteStudent(id);
        return "Student By ID: "+id+" Deleted";
    }

    @PutMapping("/{id}")
    public String updateStudent(@PathVariable("id")String id ,@RequestBody StudentRequest request ){
        studentService.UpdateStudent(id,request);
        return "Student By ID: "+id+" Updated";
    }

}
