package com.stronglover.MadrassaApp.student;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class StudentRequest {
    private final String fullname;
    private final String email;
    private final LocalDateTime birthDate;
}
