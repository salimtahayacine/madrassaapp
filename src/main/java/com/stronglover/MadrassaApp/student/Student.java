package com.stronglover.MadrassaApp.student;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Document(collection = "student")
public class Student {
    @Id
    private  String id;
    private  String fullname;
    private  String email;
    private LocalDateTime birthdate;
    private NiveauScolaire niveau;

    public Student(String fullname,
                   String email,
                   LocalDateTime birthdate,
                   NiveauScolaire niveau) {
        this.fullname = fullname;
        this.email = email;
        this.birthdate = birthdate;
        this.niveau = niveau;
    }
}
