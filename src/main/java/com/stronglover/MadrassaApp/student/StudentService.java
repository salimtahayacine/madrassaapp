package com.stronglover.MadrassaApp.student;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;
    public String addStudent(StudentRequest request ,Student student){

        //System.out.println("request:"+request);
        boolean isStudentExist = studentRepository.findByEmail(request.getEmail()).isPresent();

        if(isStudentExist){
            throw new IllegalStateException("Email already Taken");
        }
         student.setFullname(request.getFullname());
         student.setEmail(request.getEmail());
         student.setNiveau(NiveauScolaire.PREMIER);
         student.setBirthdate(request.getBirthDate());

         studentRepository.save(student);
        return "Added Successfuly";
    }
    public void DeleteStudent(String id){
       boolean isExist = studentRepository.existsById(id);
       if(!isExist){
          throw  new IllegalStateException("Student with id"+id+" not founded");
       }
       studentRepository.deleteById(id);
    }

    public void UpdateStudent(String id ,StudentRequest request){

        boolean isExist = studentRepository.existsById(id);
        if(!isExist){
            throw  new IllegalStateException("Student with id"+id+" not founded");
        }
        Student student = new Student();
        student = studentRepository.findById(id).orElseThrow();
        student.setEmail(request.getEmail());
        student.setFullname(request.getFullname());
        student.setBirthdate(request.getBirthDate());

        studentRepository.save(student);
    }
}
